use crate::Vec4;
use crate::ops::Cross;

/// ```
/// use gfxmath_vec4::ops::Cross;
/// use gfxmath_vec4::Vec4;
///
/// let a = Vec4::<f32>::new(1.0, 3.0, 2.5, 0.0);
/// let b = Vec4::all(2.0);
///
/// let res = a.cross(b);
/// 
/// assert_eq!(1.0, res.x);
/// assert_eq!(3.0, res.y);
/// assert_eq!(-4.0, res.z);
/// assert_eq!(0.0, res.w);
/// ```
#[opimps::impl_ops(Cross)]
fn cross(self: Vec4<f32>, rhs: Vec4<f32>) -> Vec4<f32> {
    let l = self.as_slice();
    let r = rhs.as_slice();

    Vec4::new(
        l[1] * r[2] - r[1] * l[2],
        l[2] * r[0] - r[2] * l[0],
        l[0] * r[1] - r[0] * l[1],
        0.0
    )
}

/// ```
/// use gfxmath_vec4::ops::Cross;
/// use gfxmath_vec4::Vec4;
///
/// let a = Vec4::<f64>::new(1.0, 3.0, 2.5, 0.0);
/// let b = Vec4::all(2.0);
///
/// let res = a.cross(b);
/// 
/// assert_eq!(1.0, res.x);
/// assert_eq!(3.0, res.y);
/// assert_eq!(-4.0, res.z);
/// assert_eq!(0.0, res.w);
/// ```
#[opimps::impl_ops(Cross)]
fn cross(self: Vec4<f64>, rhs: Vec4<f64>) -> Vec4<f64> {
    let l = self.as_slice();
    let r = rhs.as_slice();

    Vec4::new(
        l[1] * r[2] - r[1] * l[2],
        l[2] * r[0] - r[2] * l[0],
        l[0] * r[1] - r[0] * l[1],
        0.0
    )
}
