use crate::Vec4;
use core::ops::Div;

/// Scalar division with vector
/// 
/// ```
/// use gfxmath_vec4::Vec4;
/// 
/// let a = Vec4::<f32>::new(0.5, 2.5, -2.5, 3.0);
/// let b = Vec4::from(a / 2.0);
/// 
/// assert_eq!( 0.25, b.x);
/// assert_eq!( 1.25, b.y);
/// assert_eq!(-1.25, b.z);
/// assert_eq!( 1.5 , b.w);
/// ```
#[opimps::impl_ops_rprim(Div)]
fn div<T>(self: Vec4<T>, rhs: T) -> Vec4<T> where T: Div<Output = T> + Copy {
    let l = self.as_slice();
    return Vec4::new(
        l[0] / rhs,
        l[1] / rhs,
        l[2] / rhs,
        l[3] / rhs
    )
}
