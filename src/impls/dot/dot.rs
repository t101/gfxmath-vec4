use core::ops::{Add, Mul};
use crate::ops::Dot;
use crate::Vec4;

/// ```
/// use gfxmath_vec4::{Vec4, ops::Dot};
/// 
/// let a = Vec4::new(3.0, 4.0, 5.0, 3.0);
/// let b = Vec4::new(2.0, 1.0, 3.0, 3.0);
/// 
/// let res = a.dot(b);
/// 
/// assert_eq!(25.0, res);
/// ```
#[opimps::impl_ops(Dot)]
fn dot<T>(self: Vec4<T>, rhs: Vec4<T>) -> T
where T: Add<Output = T>
    + Mul<Output = T>
    + Copy 
{
    let l = self.as_slice();
    let r = rhs.as_slice();

    l[0] * r[0]
    + l[1] * r[1]
    + l[2] * r[2]
}
