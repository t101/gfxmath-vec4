use core::hash::Hash;
use crate::Vec4;


impl Hash for Vec4<f32> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        let s = self.as_slice();

        s.iter().for_each(|i| {
            unsafe { (*(i as *const _ as *const u32)).hash(state) };
        });
    }
}

impl Hash for Vec4<f64> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        let s = self.as_slice();

        s.iter().for_each(|i| {
            unsafe { (*(i as *const _ as *const u32)).hash(state) };
        });
    }
}

impl Hash for Vec4<i32> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        let s = self.as_slice();
        s.iter().for_each(|i| { i.hash(state); });
    }
}

impl Hash for Vec4<i64> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        let s = self.as_slice();
        s.iter().for_each(|i| { i.hash(state); });
    }
}

impl Hash for Vec4<u32> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        let s = self.as_slice();
        s.iter().for_each(|i| { i.hash(state); });
    }
}

impl Hash for Vec4<u64> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        let s = self.as_slice();
        s.iter().for_each(|i| { i.hash(state); });
    }
}
