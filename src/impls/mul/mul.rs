use crate::Vec4;
use core::ops::Mul;

/// ```
/// use gfxmath_vec4::Vec4;
/// 
/// let mut a = Vec4::<f32>::new(1.5, 2.5, -2.0, 5.0);
/// let b = Vec4::<f32>::new(4.0, 2.0,  4.0, 5.0);
/// let c = Vec4::from(&a * &b);
/// 
/// a.x = 2.0;
/// 
/// assert_eq!( 6.0, c.x);
/// assert_eq!( 5.0, c.y);
/// assert_eq!(-8.0, c.z);
/// assert_eq!(25.0, c.w);
/// 
/// let c = Vec4::from(a * b);
///
/// assert_eq!( 8.0, c.x);
/// assert_eq!( 5.0, c.y);
/// assert_eq!(-8.0, c.z);
/// assert_eq!(25.0, c.w);
/// ```
#[opimps::impl_ops(Mul)]
#[inline]
fn mul<T>(self: Vec4<T>, rhs: Vec4<T>) -> Vec4<T> where T: Mul<Output = T> + Copy {
    let l = self.as_slice();
    let r = rhs.as_slice();
    Vec4::new(
        l[0] * r[0],
        l[1] * r[1],
        l[2] * r[2],
        l[3] * r[3],
    )
}

/// Scalar multiplication with vector
/// 
/// ```
/// use gfxmath_vec4::Vec4;
/// 
/// let a = Vec4::<f32>::new(0.5, 2.5, -2.5, 4.0);
/// let b = Vec4::from(a * 2.0);
/// 
/// assert_eq!( 1.0, b.x);
/// assert_eq!( 5.0, b.y);
/// assert_eq!(-5.0, b.z);
/// assert_eq!( 8.0, b.w);
/// ```
#[opimps::impl_ops_rprim(Mul)]
fn mul<T>(self: Vec4<T>, rhs: T) -> Vec4<T> where T: Mul<Output = T> + Copy {
    return Vec4::new(self.x * rhs, self.y * rhs, self.z * rhs, self.w * rhs);
}

/// Scalar multiplication with vector
/// 
/// ```
/// use gfxmath_vec4::Vec4;
/// 
/// let a = Vec4::<f32>::new(0.5, 2.5, -2.5, 4.0);
/// let b = Vec4::from(2.0 * a);
/// 
/// assert_eq!( 1.0, b.x);
/// assert_eq!( 5.0, b.y);
/// assert_eq!(-5.0, b.z);
/// assert_eq!( 8.0, b.w);
/// ```
#[opimps::impl_ops_lprim(Mul)]
fn mul(self: f32, rhs: Vec4<f32>) -> Vec4<f32> {
    return Vec4::new(self * rhs.x, self * rhs.y, self * rhs.z, self * rhs.w);
}

/// Scalar multiplication with vector
/// 
/// ```
/// use gfxmath_vec4::Vec4;
/// 
/// let a = Vec4::<f64>::new(0.5, 2.5, -2.5, 4.0);
/// let b = Vec4::from(2.0 * a);
/// 
/// assert_eq!( 1.0, b.x);
/// assert_eq!( 5.0, b.y);
/// assert_eq!(-5.0, b.z);
/// assert_eq!( 8.0, b.w);
/// ```
#[opimps::impl_ops_lprim(Mul)]
fn mul(self: f64, rhs: Vec4<f64>) -> Vec4<f64> {
    return Vec4::new(self * rhs.x, self * rhs.y, self * rhs.z, self * rhs.w);
}

/// Scalar multiplication with vector
/// 
/// ```
/// use gfxmath_vec4::Vec4;
/// 
/// let a = Vec4::<i32>::new(5, 2, -2, 4);
/// let b = Vec4::from(2 * a);
/// 
/// assert_eq!( 10, b.x);
/// assert_eq!( 4, b.y);
/// assert_eq!(-4, b.z);
/// assert_eq!( 8, b.w);
/// ```
#[opimps::impl_ops_lprim(Mul)]
fn mul(self: i32, rhs: Vec4<i32>) -> Vec4<i32> {
    return Vec4::new(self * rhs.x, self * rhs.y, self * rhs.z, self * rhs.w);
}

/// Scalar multiplication with vector
/// 
/// ```
/// use gfxmath_vec4::Vec4;
/// 
/// let a = Vec4::<i64>::new(5, 2, -2, 4);
/// let b = Vec4::from(2 * a);
/// 
/// assert_eq!( 10, b.x);
/// assert_eq!(  4, b.y);
/// assert_eq!( -4, b.z);
/// assert_eq!(  8, b.w);
/// ```
#[opimps::impl_ops_lprim(Mul)]
fn mul(self: i64, rhs: Vec4<i64>) -> Vec4<i64> {
    return Vec4::new(self * rhs.x, self * rhs.y, self * rhs.z, self * rhs.w);
}
// End of scalar multiplication
