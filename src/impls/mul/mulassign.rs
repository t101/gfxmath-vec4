use crate::Vec4;
use core::ops::MulAssign;

/// ```
/// use gfxmath_vec4::Vec4;
/// 
/// let mut a = Vec4::<f32>::new(1.5, 2.5, -2.0, 3.0);
/// let b = Vec4::<f32>::new(4.0, 2.0,  4.0, 4.0);
/// a *= b;
/// 
/// assert_eq!( 6.0, a.x);
/// assert_eq!( 5.0, a.y);
/// assert_eq!(-8.0, a.z);
/// assert_eq!(12.0, a.w);
/// 
/// ```
#[opimps::impl_ops_assign(MulAssign)]
#[inline]
fn mul_assign<T>(self: Vec4<T>, rhs: Vec4<T>) where T: MulAssign<T> + Copy {
    let l = self.as_mut_slice();
    let r = rhs.as_slice();

    l[0] *= r[0];
    l[1] *= r[1];
    l[2] *= r[2];
    l[3] *= r[3];
}

/// ```
/// use gfxmath_vec4::Vec4;
/// 
/// let mut a = Vec4::<f32>::new(1.5, 2.5, -2.0, 9.0);
/// a *= 2.0;
/// 
/// assert_eq!( 3.0, a.x);
/// assert_eq!( 5.0, a.y);
/// assert_eq!(-4.0, a.z);
/// assert_eq!(18.0, a.w); 
/// ```
#[opimps::impl_op_assign(MulAssign)]
#[inline]
fn mul_assign<T>(self: Vec4<T>, rhs: T) where T: MulAssign<T> + Copy {
    let l = self.as_mut_slice();

    l[0] *= rhs;
    l[1] *= rhs;
    l[2] *= rhs;
    l[3] *= rhs;
}
