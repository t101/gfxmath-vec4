use core::ops::Neg;
use crate::Vec4;

/// ```
/// use gfxmath_vec4::ops::Cross;
/// use gfxmath_vec4::Vec4;
///
/// let a = Vec4::new(1.0, 3.0, 2.5, -3.0);
///
/// let res = -a;
/// 
/// assert_eq!(-1.0, res.x);
/// assert_eq!(-3.0, res.y);
/// assert_eq!(-2.5, res.z);
/// assert_eq!( 3.0, res.w);
/// ```
#[opimps::impl_uni_ops(Neg)]
fn neg<T>(self: Vec4<T>) -> Vec4<T>
where T: Neg<Output = T> 
    + Copy 
{
    let l = self.as_slice();

    Vec4::new(
        -l[0],
        -l[1],
        -l[2],
        -l[3]
    )
}
