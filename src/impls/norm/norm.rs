use crate::Vec4;
use crate::ops::Norm;

/// ```
/// use gfxmath_vec4::ops::Norm;
/// use gfxmath_vec4::Vec4;
/// 
/// let a = Vec4::<f32>::new(3.0, 4.0, 0.0, 0.0);
/// let an = a.norm().unwrap();
/// 
/// assert_eq!(3.0/5.0, an.x);
/// assert_eq!(4.0/5.0, an.y);
/// assert_eq!(0.0, an.z);
/// assert_eq!(0.0, an.w);
/// ```
#[opimps::impl_uni_ops(Norm)]
fn norm(self: Vec4<f32>) -> Option<Vec4<f32>> {
    let l = self.as_slice();

    let len = l[0] * l[0]
         + l[1] * l[1]
         + l[2] * l[2]
         + l[3] * l[3];
    
    let len = len.sqrt();
    if len == 0.0 { return None; }

    Some(
        Vec4::new(
            l[0]/len,
            l[1]/len,
            l[2]/len,
            l[3]/len,
        )
    )
}

/// ```
/// use gfxmath_vec4::ops::Norm;
/// use gfxmath_vec4::Vec4;
/// 
/// let a = Vec4::<f64>::new(3.0, 4.0, 0.0, 0.0);
/// let an = a.norm().unwrap();
/// 
/// assert_eq!(3.0/5.0, an.x);
/// assert_eq!(4.0/5.0, an.y);
/// assert_eq!(0.0, an.z);
/// assert_eq!(0.0, an.w);
/// ```
#[opimps::impl_uni_ops(Norm)]
fn norm(self: Vec4<f64>) -> Option<Vec4<f64>> {
    let l = self.as_slice();

    let len = l[0] * l[0]
         + l[1] * l[1]
         + l[2] * l[2]
         + l[3] * l[3];
    
    let len = len.sqrt();
    if len == 0.0 { return None; }

    Some(
        Vec4::new(
        l[0]/len,
        l[1]/len,
        l[2]/len,
        l[3]/len
        )
    )
}
