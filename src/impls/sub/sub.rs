use core::ops::Sub;
use crate::Vec4;

/// 
/// ```
/// use gfxmath_vec4::Vec4;
/// 
/// let mut a = Vec4::<f32>::new(0.5, 2.5, -2.5, 3.0);
/// let b = Vec4::<f32>::new(1.5, 2.5,  2.0, 4.0);
/// let c = Vec4::from(&a - &b);
/// 
/// a.x = 1.0;
/// 
/// assert_eq!(-1.0, c.x);
/// assert_eq!( 0.0, c.y);
/// assert_eq!(-4.5, c.z);
/// assert_eq!(-1.0, c.w);
/// 
/// let c2 = Vec4::from(a - b);
///
/// assert_eq!(-0.5, c2.x);
/// assert_eq!( 0.0, c2.y);
/// assert_eq!(-4.5, c2.z);
/// assert_eq!(-1.0, c2.w);
/// ```
#[opimps::impl_ops(Sub)]
fn sub<T>(self: Vec4<T>, rhs: Vec4<T>) -> Vec4<T> where T: Sub<Output = T> + Copy {
    let l = self.as_slice();
    let r = rhs.as_slice();

    Vec4::new(
        l[0] - r[0],
        l[1] - r[1],
        l[2] - r[2],
        l[3] - r[3],
    )
}

/// ```
/// use gfxmath_vec4::Vec4;
/// 
/// let v = Vec4::new(1.0, 2.0, 3.0, 4.0);
///
/// let res = v - 3.0;
/// 
/// assert_eq!(-2.0, res.x);
/// assert_eq!(-1.0, res.y);
/// assert_eq!( 0.0, res.z);
/// assert_eq!( 1.0, res.w);
/// ```
#[opimps::impl_ops_rprim(Sub)]
fn sub<T>(self: Vec4<T>, rhs: T) -> Vec4<T> where T: Sub<Output = T> + Copy {
    let l = self.as_slice();
    Vec4::new(
        l[0] - rhs,
        l[1] - rhs,
        l[2] - rhs,
        l[3] - rhs
    )
}

/// ```
/// use gfxmath_vec4::Vec4;
/// 
/// let v = Vec4::<f32>::new(1.0, 2.0, 3.0, 4.0);
/// 
/// let res = 3.0 - v;
/// 
/// assert_eq!( 2.0, res.x);
/// assert_eq!( 1.0, res.y);
/// assert_eq!( 0.0, res.z);
/// assert_eq!(-1.0, res.w);
/// ```
#[opimps::impl_ops_lprim(Sub)]
fn sub(self: f32, rhs: Vec4<f32>) -> Vec4<f32> {
    let r = rhs.as_slice();
    Vec4::new(
        self - r[0],
        self - r[1],
        self - r[2],
        self - r[3]
    )
}


/// ```
/// use gfxmath_vec4::Vec4;
/// 
/// let v = Vec4::<f64>::new(1.0, 2.0, 3.0, 4.0);
/// 
/// let res = 3.0 - v;
/// 
/// assert_eq!( 2.0, res.x);
/// assert_eq!( 1.0, res.y);
/// assert_eq!( 0.0, res.z);
/// assert_eq!(-1.0, res.w);
/// ```
#[opimps::impl_ops_lprim(Sub)]
fn sub(self: f64, rhs: Vec4<f64>) -> Vec4<f64> {
    let r = rhs.as_slice();
    Vec4::new(
        self - r[0],
        self - r[1],
        self - r[2],
        self - r[3]
    )
}

/// ```
/// use gfxmath_vec4::Vec4;
/// 
/// let v = Vec4::<i32>::new(1, 2, 3, 4);
/// 
/// let res = 3 - v;
/// 
/// assert_eq!( 2, res.x);
/// assert_eq!( 1, res.y);
/// assert_eq!( 0, res.z);
/// assert_eq!(-1, res.w);
/// 
/// ```
#[opimps::impl_ops_lprim(Sub)]
fn sub(self: i32, rhs: Vec4<i32>) -> Vec4<i32> {
    let r = rhs.as_slice();
    Vec4::new(
        self - r[0],
        self - r[1],
        self - r[2],
        self - r[3]
    )
}

/// ```
/// use gfxmath_vec4::Vec4;
/// 
/// let v = Vec4::<i64>::new(1, 2, 3, 4);
/// 
/// let res = 3 - v;
/// 
/// assert_eq!( 2, res.x);
/// assert_eq!( 1, res.y);
/// assert_eq!( 0, res.z);
/// assert_eq!(-1, res.w);
/// ```
#[opimps::impl_ops_lprim(Sub)]
fn sub(self: i64, rhs: Vec4<i64>) -> Vec4<i64> {
    let r = rhs.as_slice();
    Vec4::new(
        self - r[0],
        self - r[1],
        self - r[2],
        self - r[3]
    )
}
