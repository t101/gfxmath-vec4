use core::ops::SubAssign;
use crate::Vec4;

/// 
/// ```
/// use gfxmath_vec4::Vec4;
/// 
/// let mut a = Vec4::<f32>::new(0.5, 2.5, -2.5, 4.0);
/// let mut b = Vec4::<f32>::new(1.5, 2.5,  2.0, 3.0);
/// a -= b;
/// 
/// assert_eq!(-1.0, a.x);
/// assert_eq!( 0.0, a.y);
/// assert_eq!(-4.5, a.z);
/// assert_eq!( 1.0, a.w);
/// ```
#[opimps::impl_ops_assign(SubAssign)]
#[inline]
fn sub_assign<T>(self: Vec4<T>, rhs: Vec4<T>) where T: SubAssign<T> + Copy {
    let l = self.as_mut_slice();
    let r = rhs.as_slice();

    l[0] -= r[0];
    l[1] -= r[1];
    l[2] -= r[2];
    l[3] -= r[3];
}

/// 
/// ```
/// use gfxmath_vec4::Vec4;
/// 
/// let mut a = Vec4::<f32>::new(0.5, 2.5, -2.5, 3.0);
/// 
/// a -= -1.0;
/// 
/// assert_eq!( 1.5, a.x);
/// assert_eq!( 3.5, a.y);
/// assert_eq!(-1.5, a.z);
/// assert_eq!( 4.0, a.w);
/// ```
#[opimps::impl_op_assign(SubAssign)]
#[inline]
fn sub_assign<T>(self: Vec4<T>, rhs: T) where T: SubAssign<T> + Copy {
    let l = self.as_mut_slice();

    l[0] -= rhs;
    l[1] -= rhs;
    l[2] -= rhs;
    l[3] -= rhs;
}
