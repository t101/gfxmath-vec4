#![doc = include_str!("../README.md")]

mod vec4;
pub use vec4::Vec4;

#[path = "ops/lib.rs"]
pub mod ops;

#[path = "impls/lib.rs"]
mod impls;
