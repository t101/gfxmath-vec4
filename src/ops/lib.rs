mod cross;
pub use cross::*;

mod dot;
pub use dot::*;

mod norm;
pub use norm::*;
