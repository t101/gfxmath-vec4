#[repr(C)]
#[derive(Debug, Clone, PartialEq)]
pub struct Vec4<T> {
    pub x: T,
    pub y: T,
    pub z: T,
    pub w: T
}

impl <T> Vec4 <T> {
    pub fn new(x: T, y: T, z: T, w: T) -> Self {
        Self { x, y, z, w }
    }

    pub fn as_ptr(&self) -> *const T {
        self as *const _ as *const T
    }

    pub fn as_mut_ptr(&mut self) -> *mut T {
        self as *mut _ as *mut T
    }

    ///
    /// ```
    /// use gfxmath_vec4::Vec4;
    /// 
    /// let a = Vec4::<f32>::new(1.0, 2.0, 3.0, 4.0);
    /// 
    /// let a_slice = a.as_slice();
    /// 
    /// assert_eq!(1.0, a_slice[0]);
    /// assert_eq!(2.0, a_slice[1]);
    /// assert_eq!(3.0, a_slice[2]);
    /// assert_eq!(4.0, a_slice[3]);
    /// ```
    pub fn as_slice(&self) -> &[T] {
        unsafe { std::slice::from_raw_parts(self.as_ptr(), 4) }
    }

    ///
    /// ```
    /// use gfxmath_vec4::Vec4;
    /// 
    /// let mut a = Vec4::<f32>::new(1.0, 2.0, 3.0, 4.0);
    /// 
    /// {
    ///     let a_slice = a.as_mut_slice();
    /// 
    ///     assert_eq!(1.0, a_slice[0]);
    ///     assert_eq!(2.0, a_slice[1]);
    ///     assert_eq!(3.0, a_slice[2]);
    ///     assert_eq!(4.0, a_slice[3]);
    /// 
    ///     a_slice[2] = 108.0;
    ///     assert_eq!(108.0, a_slice[2]); 
    /// }
    /// 
    /// a.x = 4.5;
    /// assert_eq!(4.5, a.x);
    /// 
    /// let a_slice = a.as_mut_slice();
    /// 
    /// assert_eq!(4.5, a_slice[0]);
    /// assert_eq!(2.0, a_slice[1]);
    /// assert_eq!(108.0, a_slice[2]);
    /// assert_eq!(4.0, a_slice[3]);
    /// ```
    pub fn as_mut_slice(&mut self) -> &mut [T] {
        unsafe { std::slice::from_raw_parts_mut(self.as_mut_ptr(), 4) }
    }
}

impl <T> Vec4<T> where T: Clone {
    /// ```
    /// use gfxmath_vec4::Vec4;
    /// 
    /// let v = Vec4::<f32>::all(2.5);
    /// 
    /// assert_eq!(2.5, v.x);
    /// assert_eq!(2.5, v.y);
    /// assert_eq!(2.5, v.z);
    /// assert_eq!(2.5, v.w);
    /// ```
    pub fn all(val: T) -> Self {
        Self::new(val.clone(), val.clone(), val.clone(), val.clone())
    }
}

impl <T> From<(T, T, T, T)> for Vec4<T> {
    fn from(value: (T, T, T, T)) -> Self {
        Vec4 {
            x: value.0,
            y: value.1,
            z: value.2,
            w: value.3
        }
    }
}

#[macro_export]
macro_rules! vec4 {
    ($x: expr, $y: expr, $z: expr, $w: expr) => {
        {
            Vec4::new($x, $y, $z, $w)
        }
    };
}
